import QtQuick 2.7
import QtQuick.Particles 2.0

/* Needed for the image provider */
import org.kde.plasma.asciiquarium 1.0

Image {
    id: seaweed

    function randBetween(l, r) {
        return Math.floor(l + (Math.random() * (r - l)));
    }

    source: "image://org.kde.plasma.asciiquarium/from_left/seaweed"
    cache: false

    Timer {
        interval: randBetween(500, 700)
	repeat: true
	running: true
	triggeredOnStart: randBetween(0, 1)
	onTriggered: seaweed.mirror ^= 1
    }
}
